import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
@Input() changes  = " Lorem ipsum dolor sit amet consectetur adipiscing elit lectus a orci aliquet, lacus eu molestie vehicula consequat penatibus morbi tempus pretium."
@Input() title;

  public message() { 
    alert(this.changes + " " + " Do you continue?...");
  }

  ngOnInit() {
  }

}
