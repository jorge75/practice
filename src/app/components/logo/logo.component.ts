import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css']
})
export class LogoComponent implements OnInit {
@Input() changeLogo;
  constructor() { }

  ngOnInit() {
  }

}
