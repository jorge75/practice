import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  
})
export class NavbarComponent implements OnInit {
 pages= ['home', 'services', 'about', 'contact', 'fact'];
  constructor() { 
    this.pages.reverse();
  }

  ngOnInit() {
  }

}
